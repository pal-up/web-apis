package com.palup.crud;

import com.palup.dao.LabelDAO;
import com.palup.daomysql.LabelDAOMySQL;
import com.palup.entity.Label;
import com.palup.entity.LabelCreationDetails;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;

/**
 * CRUD support for labels
 * <p/>
 * Created by Kuldeep Yadav on 08-Dec-15.
 */
@Path("/label")
public class LabelCRUD {
    /**
     * User name to access database.
     */
    private String user = "kuldeep";
    /**
     * Password for the user
     */
    private String password = "kd5534kd";
    /**
     * Database to access
     */
    private String url = "jdbc:mysql://localhost/paldb";

    /**
     * Dummy function to test WEB-Services
     *
     * @return Http OK with a message
     */
    @GET
    public Response greet() {
        return Response.ok("Welcome to Pal-up Label Web APIs !!").build();
    }

    /**
     * @param label creation details
     * @return Http OK response if label created successfully
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Label create(final LabelCreationDetails label) throws SQLException, ClassNotFoundException {

        LabelDAO labelDAO = new LabelDAOMySQL(user, password, url);
        Label newLabel = labelDAO.createLabel(label);
        labelDAO.setLabelRelation(label);
        return newLabel;
    }

    @GET
    @Path("/read/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Label> getAllLabels() throws SQLException, ClassNotFoundException {

        LabelDAO labelDAO = new LabelDAOMySQL(user, password, url);
        return labelDAO.readLabels();
    }
}
