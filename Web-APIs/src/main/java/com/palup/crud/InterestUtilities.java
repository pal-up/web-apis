package com.palup.crud;

import com.palup.dao.LabelDAO;
import com.palup.dao.UserDAO;
import com.palup.daomysql.LabelDAOMySQL;
import com.palup.daomysql.UserDAOMySQL;
import com.palup.entity.Interest;
import com.palup.entity.Label;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Utilities related to interest
 * <p>
 * Created by kuldeep on 29/12/15.
 */
public class InterestUtilities {

    /**
     * Log debug info
     */
    private Logger logger = Logger.getLogger("Event CRUD");

    /**
     * @param email id of user
     * @return list containing user interests
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public List<Interest> userInterests(String email) throws SQLException, ClassNotFoundException {

        LabelDAO labelDAO = new LabelDAOMySQL();
        List<Label> labels = labelDAO.readLabels();

        UserDAO userDAO = new UserDAOMySQL();
        List<String> userLabels = userDAO.userInterest(email);

        Set userLabelSet = new HashSet<String>(userLabels);
        List<Interest> interests = new ArrayList<Interest>();
        for (Label label : labels) {
            if (userLabelSet.contains(label.getId())) {
                interests.add(new Interest(label, true));
            } else {
                interests.add(new Interest(label, false));
            }
        }

        return interests;
    }

    /**
     * @param email of user
     * @return list of labels for concerned user
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public List<String> userLabels(String email) throws SQLException, ClassNotFoundException {
        UserDAO userDAO = new UserDAOMySQL();
        return userDAO.userInterest(email);
    }

    /**
     * Update interest of user with given email id
     *
     * @param email              id of user
     * @param updatedInterestSet set of interests
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void updateInterests(String email, Set<String> updatedInterestSet) throws SQLException, ClassNotFoundException {

        UserDAO userDAO = new UserDAOMySQL();
        List<String> existingInterests = userDAO.userInterest(email);
        Set<String> existingInterestSet = new HashSet<String>(existingInterests);

        Set<String> toAdd = new HashSet<String>();
        for (String label : updatedInterestSet) {
            if (!existingInterestSet.contains(label)) {
                toAdd.add(label);
            } else {
                existingInterestSet.remove(label);
            }
        }
        // existingInterestSet is left with labels those to be removed
        userDAO.addInterest(email, new ArrayList<String>(toAdd));
        userDAO.removeInterest(email, new ArrayList<String>(existingInterestSet));

    }
}
