package com.palup.crud;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Junit test for EventCRUD
 *
 * @see EventCRUD
 * <p/>
 * Created by Kuldeep Yadav on 27-Nov-15.
 */
public class EventCRUDTest {

    // @Test
    public void testAllEvents() throws Exception {

        System.out.println("Hello world");

        URL url = new URL("http://localhost:8080/event/allevents");
        final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setRequestProperty("Contant-length", "0");
        urlConnection.setUseCaches(false);
        urlConnection.setAllowUserInteraction(false);
        urlConnection.setConnectTimeout(10000);
        urlConnection.connect();

        int status = urlConnection.getResponseCode();

        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line);
            }
            bufferedReader.close();
            System.out.println(builder.toString());
        } else {
            System.out.println(status);
        }

    }
}